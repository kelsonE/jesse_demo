const CREATE_BUTTON = document.querySelector("#createBtn");
const CARDS_CONTAINER = document.querySelector("#cardsContainer");

const noteItems = [];

const deleteHandler = (e)=>{
    //parse int because dom items are always strings
 const cardId = parseInt(e.target.parentElement.id);

 //find item in arr
 let foundCardIndex = null;

 for(let i = 0; i < noteItems.length; i++){
     const currentCard = noteItems[i];
     
     if(currentCard.id === cardId){
         //save the index
         foundCardIndex = i
         break;
     }
 }

 //remove item from array
 noteItems.splice(foundCardIndex, 1);

 //rebuild dom
 createItems()
}

const createItems = () => {
    //we are going to remove all the cards before adding new ones
    const cards = document.querySelectorAll('.card');
    for(let i = 0; i < cards.length; i++ ){
        cards[i].remove();
    }

    console.log(noteItems)
    //add items from noteItems arr
    for(let i = 0; i < noteItems.length; i++){
        const currentNote = noteItems[i];
        
        const title = document.createElement('p');
        title.innerHTML = currentNote.title;
        const description = document.createElement('p');
        description.innerHTML = currentNote.description;

        const div = document.createElement('div');
        div.id = currentNote.id;
        const list = div.classList;
        list.add("card");

        const deleteBtn = document.createElement('button');
        deleteBtn.innerHTML = 'Delete';
        deleteBtn.classList.add('deletebtn');
        deleteBtn.addEventListener('click', deleteHandler)

        div.appendChild(title);
        div.appendChild(description);
        div.appendChild(deleteBtn);

        CARDS_CONTAINER.appendChild(div)
    }

}

CREATE_BUTTON.addEventListener('click', function(e){
    e.preventDefault();

    //Add new item to array. This is hardcoded, but yours wont be
    noteItems.push({
        id: Date.now(),
        title: `This is item num - ${noteItems.length}`,
        description: "blah blah"
    })

    createItems();
})
